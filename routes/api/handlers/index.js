var keystone = require('keystone');
var os= require('os');
var exec = require('child_process').exec;
var Chart = keystone.list('Chart').model;
var Host = keystone.list('Host').model;
var dns = require('dns');
var geoip=require('geoip-lite');

var handlers = {
  getCharts: function(req, res) {
    Chart
    .find()
    .populate('item')
    .exec(function(err, charts) {
      if(err) {
        console.log(err);
        res.status(500).send('DB Error');
      }
      res.status(200).send(charts);
    });
  },

  getOS: function(req, res) {
    var locals=res.locals;
    locals.os={};
    locals.os.arch=os.arch();
    locals.os.platform=os.platform();
    locals.os.release=os.release();
    locals.os.freemem=os.freemem();
    locals.os.totalmem=os.totalmem();
    locals.os.uptime=os.uptime();
    locals.os.loadavg=os.loadavg();
    locals.os.cpus=os.cpus();
    res.status(200).send(locals.os);
  },
  
  getCPUTemperature: function(req,res){
    var locals=res.locals;
    locals.board={};
    child = exec("cat /sys/class/thermal/thermal_zone0/temp", function (error, stdout, stderr){
      if(error!==null){
        locals.board.cputemperature=null;
        res.status(200).send(locals.board);
      }
      else{
        var temp = (parseFloat(stdout)/1000).toFixed(2);
        locals.board.cputemperature=temp;
        res.status(200).send(locals.board);        
      }
    })   
  },

  getGeoIp: function(req,res){
    if (req.query.host){
      try {
          var w3 = dns.lookup(req.query.host, function (err, addresses, family) {
          var geo = geoip.lookup(addresses);
          if (geo){
            res.json({host:req.query.host,ip:addresses,geoip:geo});
          }
          else {
          res.json({host:req.query.host,ip:addresses});
          }
         });
      }
      catch(err){
         var geo = {host:req.query.host,ip:req.query.host,geoip:{ll:[-6.170677, 106.835730]}}
         res.json(geo);
      }  
    }      
  },

  getTimeString: function(req,res){
    var locals=res.locals;
    var t = new Date();
    locals.timeString=t.toUTCString();
    res.status(200).send({time:locals.timeString});
  },

  getHosts: function(req,res){
    var ret=[]
    Host
    .find()
    .exec(function(err, hosts) {
      if(err){
        res.status(500).send('DB Error');
      }
      hosts.forEach(element => {
        var addr=element.address+':'+element.vncport+'/vnc_lite.html';
        ret.push({text:element.title,href:addr})
      });
      res.status(200).send({data:ret});
    })
  }
}

module.exports = handlers;
