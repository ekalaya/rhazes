var keystone = require('keystone');
var os = require('os');
exports = module.exports = function (req, res) {
    var view = new keystone.View(req, res);
    var locals = res.locals;
	view.on('init',function(next){                
        locals.platform=os.platform();
        next();
    });
    view.render('status');
};