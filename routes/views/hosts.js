var keystone = require('keystone');
var publicIP = require('public-ip');
var geoip = require('geoip-lite');
var url = require('url');
var validateIP=require('validate-ip-node');
var dns=require('dns-sync');
exports = module.exports = function (req, res) {
    var view = new keystone.View(req, res);
    var locals = res.locals;
    var arr=[];
	locals.data = {
        hosts:[]
    };

	view.on('init',function(next){
		var q = keystone.list('Host').model.find();
		q.exec(function (err, result) {         
            console.log(result);
            result.forEach(element => {
                var myurl=url.parse(element.address);
                arr.push(myurl.hostname);
            });
            locals.data.hosts=arr;
            console.log(arr);            			
            next(err);            
		});
	});

    view.render('hosts');    
};