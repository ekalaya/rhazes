var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.data = {
		chart: {},
	};

	// Load the current post
	view.on('init', function (next) {
		var q = keystone.list('Chart').model.findOne({_id:req.params.id}).
		populate({
			path:'host'
		}).
		populate({
			path:'item',
			populate:{
				path: 'category',
				model:'Category'
			}
		})
		q.exec(function (err, result) {
            locals.data.chart = result;
            console.log(result);
			next(err);
		});

	});

    // Render the view
	view.render('chart');
};
 