var keystone = require('keystone');
var publicIP = require('public-ip');
var geoip = require('geoip-lite');

var pagination = function(current, total){
	var list = [];
	var pageLimit = 3;
	var upperLimit, lowerLimit;
	var currentPage = lowerLimit = upperLimit = Math.min(current, total);

	for (var b = 1; b < pageLimit && b < total;) {
	    if (lowerLimit > 1 ) {
	        lowerLimit--; b++; 
	    }
	    if (b < pageLimit && upperLimit < total) {
	        upperLimit++; b++; 
	    }
	}
	for (var i = lowerLimit; i <= upperLimit; i++) {
		list.push(i);
	}
	return list;
}

exports = module.exports = function (req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Set locals
	locals.section = 'charts';
	locals.data = {
		charts: [],
		numitems:0,
		hosts:[],
		categories:[],
		pages:[],
		prev:null,
		next:null,
		totalpages:0,
	};
	var pageno=req.params.pageno;		
	view.on('init',function(next){
		var q = keystone.list('Host').model.find();
		q.exec(function (err, result) {
			locals.data.hosts = result
			console.log(result)
			next(err);
		});
	});

	view.on('init',function(next){
		var q = keystone.list('Category').model.find();
		q.exec(function (err, result) {
			locals.data.categories = result
			next(err);
		});
	});

	view.on('init',next=>{
		publicIP.v4().then(ip => {
			var geo = geoip.lookup(ip);						
		});
		next();
	})

	view.on('init',function(next){
		// console.log(req.cookies);
		var Chart = keystone.list('Chart').model;
			user = locals.user?locals.user._id:null,
			idpublisher=user,
			perpage = locals.user?locals.user.perPage:2,
			private = locals.user?true:false,
			idcategory = req.cookies.category || 'all';
			idhost=req.cookies.host || 'all';
		console.log(locals.user);
		if (user){
			var q=Chart.find({'publisher':idpublisher});			
		}
		else{
			var q=Chart.find({'isprivate':private});
		}
		
		if (req.body && idhost && idhost!='all'){
	 	 	q.where('host',idhost)
		 }		
		q.populate({
			path:'item',
			populate:{
				path:'category',
				model:'Category'
			}
		}).
		populate('host publisher');
		q.exec(function (err, result) {			
			if (result){		
				var result2=[];				
				result.forEach(element => {
					if (String(element.item.category._id)===String(idcategory)){						
						result2.push(element);
					}
					if (idcategory=='all'){
						result2.push(element);
					}					
				});				
				locals.data.numitems=result2.length;
				var totalpages=Math.ceil(result2.length/perpage);
				locals.data.totalpages=totalpages;
				locals.data.currentpage=pageno;
				locals.data.pages=pagination(pageno,totalpages);
				result2=result2.slice((pageno-1)*perpage,pageno*perpage);
				locals.data.charts = result2;
				if (pageno!=1){
					locals.data.prev=parseInt(pageno)-1;
				}					
				if (pageno!=totalpages){
					locals.data.next=parseInt(pageno)+1;
				}					
		   }
		   next(err);
	   })	
	});
	view.render('charts');
};
 