Rhazes
======

How to install
==============

Prerequsite
-----------

* [nodejs](https://nodejs.org)
* MongoDB server
* [Netdata](https://github.com/firehol/netdata)
* [pm2](http://pm2.keymetrics.io/) (optional)
* nodemon (optional)

Install nodejs 
--------------
1. get binary package for ARM from https://nodejs.org
2. extract package and copy package to /usr/local directory
   
for example:

```
$ tar xvf node-v8.9.4-linux-armv7l.tar.xz
$ cd node-v8.9.4-linux-armv7l
$ sudo cp -R *  /usr/local

```

Install Netdata for Raspberry Pi
--------------------------------
Become root

```bash
$ sudo su
```    

Install all the packages needed to build and run netdata

```bash
    
# curl -Ss 'https://raw.githubusercontent.com/firehol/netdata-demo-site/master/install-required-packages.sh' >/tmp/kickstart.sh && bash /tmp/kickstart.sh -i netdata

```
    
press ENTER or Y as instructed


Download netdata
----------------

```bash
    
# cd /usr/src
# git clone https://github.com/firehol/netdata.git --depth=1
# cd netdata
# ./netdata-installer.sh
```    
    

Install Rhazes
--------------

1. clone this repository 

```bash

$ git clone https://gitlab.com/ekalaya/rhazes.git
```    
    
2. go to rhazes directory

```bash
$ cd rhazes
```    
    
3. install all packages needed by rhazes

```bash
$ npm install
```

4. run web server

```bash
$ npm start
```

5. Done    


DB Exporter
-----------
There is a Python script, named exporter.py (tools/exporter.py), functions as db exporter from curl (https://github.com/netdata/netdata/wiki/receiving-netdata-metrics-from-shell-scripts)

To use this script you must first install pymongo and requests lib

1. install pymongo

```bash
# pip install pymongo
```    

2. install requests

```
# pip install requests
```    

3. execute the script

```
$ python exporter.py
```

Script will populate categories and items collection into rhazes db.

ISSUES
------
There are some vulnerabilities issue in depedency packages which are needed by keystone js.
Please ignore the issues if this software will only be deployed internally
