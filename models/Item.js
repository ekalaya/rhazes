var keystone = require('keystone'),
Types = keystone.Field.Types;

var Item = new keystone.List('Item',{
    autokey: { path: 'slug', from: 'title', unique: true },
    map: { name: 'title' },
})

Item.add({
title: { type: String, required: true },
category: {type: Types.Relationship, ref: 'Category',initial:true,required:true},
description: { type: String,},
});
Item.defaultColumns = 'title, category, description'
Item.register();
