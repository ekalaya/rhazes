var keystone = require('keystone'),
Types = keystone.Field.Types;

var Host = new keystone.List('Host',{
    autokey: { path: 'slug', from: 'title', unique: true },
    map: { name: 'title' },
});

Host.add({
title: { type: String, required: true },
address: { type: String,},
vncport: {type: Types.Number, label:'VNC Port',default:6800},
netdataport: {type: Types.Number, label:'Netdata Port',default:19999}
});
Host.defaultColumns = 'title, address'
Host.register();
