var keystone = require('keystone'),
Types = keystone.Field.Types;

var Category = new keystone.List('Category',{
    autokey: { path: 'slug', from: 'title', unique: true },
    map: { name: 'title' },
});

Category.add({
title: { type: String, required: true },
description: { type: String,},
});
Category.defaultColumns = 'title, description'
Category.register();
