var mongoose = require('mongoose')
var keystone = require('keystone'),
Types = keystone.Field.Types;
var deepPopulate = require('mongoose-deep-populate')(mongoose);

var Chart = new keystone.List('Chart',{
    autokey: { path: 'slug', from: 'title', unique: true },
    map: { name: 'title' },
})

Chart.add({
title: { type: String, required: true },
item: {type: Types.Relationship, ref: 'Item', },
host: {type: Types.Relationship, ref: 'Host'},
publisher: {type: Types.Relationship, ref: 'User'},
isprivate: {type: Boolean, label: 'Private', default: true},
charttype: { type: Types.Select, options: 'google, dygraph, gauge', default: 'google' },
description: { type: String},
createdAt: {type: Date,default: Date.now},
});
Chart.defaultColumns = 'title, item, host, publisher, isprivate, createdAt'
Chart.schema.plugin(deepPopulate);
Chart.register();
