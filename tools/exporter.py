import pymongo
import requests
from pymongo.errors import DuplicateKeyError
from bson.objectid import ObjectId
url='http://localhost:19999/api/v1/allmetrics'

r=requests.get(url).text.split('\n')

client = pymongo.MongoClient("mongodb://localhost:27017/")
db = client["rhazes"]
init_categories_num=db.categories.count()
init_items_num=db.items.count()

lines=[]
for line in r:
    if 'chart:' in line:
        lines.append(line)
        
items=[]
cat_num=0
for line in lines:
    item=line.split(':')[1].split('(')[0].strip()        
    category=item.split('.')[0]
    try:      
        #insert category into db           
        _id=db.categories.insert_one({'slug':category,'title':category.upper(),'__v':0,'description':''})  
        cat_num=cat_num+1        
    except DuplicateKeyError:
        #nothing to do if duplicate key exception raised
        pass
    items.append({'category':category.upper(),'item':item})

#it's time to insert items into db
item_num=0
for item in items:
    c=db.categories.find_one({'title':item['category']})
    if c:
        _id=c['_id']
        try:
            db.items.insert_one({'slug':item['item'],'title':item['item'],'category':ObjectId(_id),'description':'','__v':0})
            item_num=item_num+1
        except DuplicateKeyError:
            pass

print("\n\nSTATISTICS")
print("\t\t\tBefore\t\t\tAfter\t\t\tInserted")
print("================================================================================")
print("number of categories\t%d\t\t\t%d\t\t\t%d"%(init_categories_num,init_categories_num+cat_num,cat_num))
print("number of items\t\t%d\t\t\t%d\t\t\t%d"%(init_items_num,init_items_num+item_num,item_num))
print("\n")

    
